public class Command {
    int droneId;
    CommandType type;
    int warehouseId;
    int productTypeId;
    int nbItems;
    int orderId;
    int nbTurnsForWait;


    //Constructor of Wait command
    public Command(int droneId, int waitTurns){
        this.droneId = droneId;
        type = CommandType.WAIT;
        this.nbTurnsForWait = waitTurns;
    }

    //Constructor of Load or Unload command
    public Command(int droneId, CommandType type, int warehouseId, int productTypeId, int nbItems){
        this.droneId = droneId;
        this.type = type;
        this.warehouseId = warehouseId;
        this.productTypeId = productTypeId;
        this.nbItems = nbItems;
    }

    //Constructor of Deliver command
    public Command(int droneId, int orderId, int productTypeId, int nbItems){
        this.droneId = droneId;
        type = CommandType.DELIVER;
        this.orderId = orderId;
        this.productTypeId = productTypeId;
        this.nbItems = nbItems;
    }

    @Override
    public String toString() {
        if(type == CommandType.DELIVER){
            return droneId + " D " + orderId + " " + productTypeId + " " + nbItems;
        }
        else if(type == CommandType.LOAD){
            return droneId + " L " + warehouseId + " " + productTypeId + " " + nbItems;
        }
        else if(type == CommandType.UNLOAD){
            return droneId + " U " + warehouseId + " " + productTypeId + " " + nbItems;
        }
        else
            return droneId + " W " + nbTurnsForWait;
    }
}