import com.ibm.net.SocketUtils;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;

public class Model {
    //private String instancePath;


    private int nbTurns;
    private int nbRows;
    private int nbColumns;
    private int nbDrones;
    private int droneCapacity;

    private int nbProducts;
    private int nbWarehouses;
    private int nbCustomerOrders;

    private Map<Integer,Integer> products=new HashMap<Integer, Integer>();
    List<Warehouse> warehouses=new ArrayList<Warehouse>();
    private List<Order> orders=new ArrayList<Order>();

    public Model(String path) {
        Scanner scanner;
        try{
            scanner=new Scanner(new FileReader(path));
        }catch (FileNotFoundException ex){
            System.out.println("File Not Found Tocard de Valentin!");
            return;
        }
        //General data
        setNbRows(scanner.nextInt());
        setNbColumns(scanner.nextInt());
        setNbDrones(scanner.nextInt());
        setNbTurns(scanner.nextInt());
        setDroneCapacity(scanner.nextInt());
        setNbProducts(scanner.nextInt());
        for (int i=0;i<getNbProducts();i++)
            products.put(i,scanner.nextInt());
        //Warehouses section
        setNbWarehouses(scanner.nextInt());
        for(int i=0;i<getNbWarehouses();i++){
            int row=scanner.nextInt();
            int column=scanner.nextInt();
            Warehouse warehouse=new Warehouse(i,row,column);
            warehouses.add(warehouse);
            for(int j=0;j<getNbProducts();j++){
                warehouse.add(j,scanner.nextInt());
            }
        }
        //Orders section
        setNbCustomerOrders(scanner.nextInt());
        for(int k=0;k<getNbCustomerOrders();k++){
            int row = scanner.nextInt();
            int column = scanner.nextInt();
            Order order = new Order(this,k,row,column);
            int nbRequiredItems = scanner.nextInt();
            for(int i=0;i<nbRequiredItems;i++){
                order.addItem(scanner.nextInt());
            }
            orders.add(order);
        }

    }


    public void displayModelStat(){

        System.out.println("--------------");
        System.out.println("Nb rows "+getNbRows());
        System.out.println("Nb columns "+getNbColumns());
        System.out.println("Nb drones "+getNbDrones());
        System.out.println("Nb turns "+getNbTurns());
        System.out.println("Max payload "+getDroneCapacity());
        System.out.println("Nb products types "+getNbProducts());
        for(Integer key:products.keySet()){
            System.out.printf(" "+products.get(key));
        }
        System.out.println("\nWarehouses (" + getNbWarehouses() + ")\n\n");
        for(Warehouse warehouse:warehouses)
            System.out.println(warehouse.toString());
        System.out.println("\nOrders ( " + getNbCustomerOrders() + ")\n\n");
        for(Order order : orders)
            System.out.println(order.toString());

        System.out.println("-------------------------------------------------");

        System.out.println(getDroneCapacity());

        int totalVolume = 0;
        for(Order order : orders){
            for(Integer productType : order.demands.keySet()){
                totalVolume+=order.demands.get(productType)*products.get(productType);
            }
        }
        System.out.println( "Average volume per order " + totalVolume/nbCustomerOrders);

        for(Order order : orders){
            Warehouse closest = order.closestWarehouses.get(0);
            System.out.println("Order #" + order.getId() + ": closest warehouse = "
                    + closest.getId() + " at distance "
                    + ModelUtils.distance(closest.getRow(),closest.getColumn(),order.getRow(),order.getColumn()));
        }
        for(Warehouse warehouse : orders.get(0).closestWarehouses){
            System.out.println("distance " + ModelUtils.distance(warehouse.getRow(),warehouse.getColumn(),orders.get(0).getRow(),orders.get(0).getColumn()));
        }

    }

    public int getNbTurns() {
        return nbTurns;
    }

    public void setNbTurns(int nbTurns) {
        this.nbTurns = nbTurns;
    }

    public int getNbRows() {
        return nbRows;
    }

    public void setNbRows(int nbRows) {
        this.nbRows = nbRows;
    }

    public int getNbColumns() {
        return nbColumns;
    }

    public void setNbColumns(int nbColumns) {
        this.nbColumns = nbColumns;
    }

    public int getNbDrones() {
        return nbDrones;
    }

    public void setNbDrones(int nbDrones) {
        this.nbDrones = nbDrones;
    }

    public int getDroneCapacity() {
        return droneCapacity;
    }

    public void setDroneCapacity(int droneCapacity) {
        this.droneCapacity = droneCapacity;
    }

    public int getNbProducts() {
        return nbProducts;
    }

    public void setNbProducts(int nbProducts) {
        this.nbProducts = nbProducts;
    }

    public int getNbWarehouses() {
        return nbWarehouses;
    }

    public void setNbWarehouses(int nbWarehouses) {
        this.nbWarehouses = nbWarehouses;
    }

    public int getNbCustomerOrders() {
        return nbCustomerOrders;
    }

    public void setNbCustomerOrders(int nbCustomerOrders) {
        this.nbCustomerOrders = nbCustomerOrders;
    }


    public Solution solve1(){
        //initiate drones
        List<DynamicDrone> drones = new ArrayList<>();
        for(int i=0;i<nbDrones;i++){
            DynamicDrone dynamicDrone = new DynamicDrone(i,warehouses.get(0).row,warehouses.get(0).column,droneCapacity);
            drones.add(dynamicDrone);
        }


        Solution solution = new Solution();
        return solution;
    }


}
