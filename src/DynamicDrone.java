import java.util.HashMap;
import java.util.Map;

public class DynamicDrone {
    int id;
    int currentTurn=0;
    int currentOrderid;
    int currentRow;
    int currentColumn;
    int remainingCapacity;
    Map<Integer,Integer> inventory = new HashMap<>();


    public DynamicDrone(int droneId, int currentRow, int currentColumn, int remainingCapacity){
        this.id=droneId;
        this.currentRow=currentRow;
        this.currentColumn=currentColumn;
        this.remainingCapacity=remainingCapacity;
    }

}
