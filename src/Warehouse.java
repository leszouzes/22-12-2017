import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Warehouse {
    int id;
    int row;
    int column;
    Map<Integer,Integer> stock =new HashMap<Integer, Integer>();
    Map<Integer,Integer> dynamicStock =new HashMap<Integer, Integer>();

    public Warehouse(int id, int row, int column) {
        this.id = id;
        this.row = row;
        this.column = column;
    }

    public int getId() {
        return id;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public int getStock(int id){
        return stock.get(id);
    }

    public void add(int id,int stockInput){
        stock.put(id,stockInput);
        dynamicStock.put(id,stockInput);
    }

    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append("Warehouse #");
        s.append(id);
        s.append("\n[");
        s.append(row);
        s.append(" , ");
        s.append(column);
        s.append("]\n");
        s.append("Stock: ");
        for(Integer key : stock.keySet()){
            s.append(" ");
            s.append(stock.get(key));
            s.append(" item(s) of product type ");
            s.append(key);
            s.append(" / ");
        }
        return s.toString();
    }
}
