import java.util.*;

public class Order {
    Model model;
    private int id;
    private int row;
    private int column;

    Map<Integer,Integer> demands = new HashMap<Integer, Integer>();
    Map<Integer,Integer> dynamicDemands = new HashMap<Integer, Integer>();
    List<Warehouse> closestWarehouses = new ArrayList<>();

    public Comparator<Warehouse> warehouseComparator = new Comparator<Warehouse>() {
        @Override
        public int compare(Warehouse o1, Warehouse o2) {
            return ModelUtils.distance(o1.getRow(),o1.getColumn(),row,column)
                    - ModelUtils.distance(o2.getRow(),o2.getColumn(),row,column);
        }
    };

    public Order(Model model, int id, int row, final int column) {
        this.id = id;
        this.row = row;
        this.column = column;
        this.model = model;
        closestWarehouses.addAll(model.warehouses);
        Collections.sort(closestWarehouses, warehouseComparator);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public void addItem(int productId){
        //If key did not exist yet, put one. Else, simply increment
        if(demands.get(productId)==null)
            demands.put(productId,1);
        else
            demands.put(productId,demands.get(productId)+1);
        if(dynamicDemands.get(productId)==null)
            dynamicDemands.put(productId,1);
        else
            dynamicDemands.put(productId,dynamicDemands.get(productId)+1);
    }

    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append("Order #");
        s.append(id);
        s.append("\n[");
        s.append(row);
        s.append(" , ");
        s.append(column);
        s.append("]\n");
        s.append("Demand: ");
        for(Integer key : demands.keySet()){
            s.append(" ");
            s.append(demands.get(key));
            s.append(" item(s) of product type ");
            s.append(key);
            s.append(" / ");
        }
        return s.toString();
    }

    public double computeScore(){
        double score = 0;
        for(Integer productTypeId : demands.keySet()){

        }
        return score;
    }


}

