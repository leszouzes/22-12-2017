import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Solution {
    List<Command> commands = new ArrayList<Command>();

    public void write(String path) throws IOException{
        FileWriter fileWriter = new FileWriter(path);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        bufferedWriter.write(commands.size()+"\n");
        for(Command command : commands){
            bufferedWriter.write(command.toString()+"\n");
        }
    }
}
